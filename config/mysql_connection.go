package config

import (
	"fmt"

	_ "github.com/jinzhu/gorm/dialects/mysql"
	"gitlab.com/aher.shiva/estern/api/common"
	"gitlab.com/aher.shiva/estern/api/models"
)

//TODO: need to move connection urls to .envrc
func OpenMysqlConnection() common.Gormw {
	db, err := common.Openw("mysql", "root:root@/estern?charset=utf8&parseTime=True&loc=Local")
	if err != nil {
		fmt.Println("failed to open mysql connection")
		panic(err)
	}

	db.AutoMigrate(&models.Hotel{}, &models.Room{}, &models.RatePlan{})
	return db
}
