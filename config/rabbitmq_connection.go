package config

import (
	"fmt"

	"github.com/streadway/amqp"
)

//TODO: need to move connection urls to .envrc
func OpenRabbitMqConnection() *amqp.Connection {
	conn, err := amqp.Dial("amqp://guest:guest@localhost:5672/")
	if err != nil {
		fmt.Println("failed to open rabbitmq connection")
		panic(err)
	}
	return conn
}
