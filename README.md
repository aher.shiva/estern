# ESTERN: RABBITMQ ASSIGNMENT

# Prerequisites
    1. Docker
    2. Go 1.12 with Go Mod enabled
    3. Mysql Docker Image
    4. RabbitMQ Docker Image

# Steps to start the service in local machine

## Download source
    git clone git@gitlab.com:aher.shiva/estern.git

## Start DB and RabbitMQ service
docker-compose up -d

### Important Note: 
wait for 4-5 mins for mysql and rabbitMq to get up and running, or else creating connection may panic.


### Install dependencies using go mod
    go mod vendor

### Build & Start ESTERN service
    go build
    ./estern

## Debugg
### Setting up gcc PATH 
if you got following error while running `go test` use command `export CGO_ENABLED="0"`

error: #runtime/cgo
exec: "gcc": executable file not found in $PATH

### Enabling Go Mod
export GO111MODULE="on"
 
## Publish Message
to publish message you can make use of collection provided in postman collection 

#### estern/postman/collection.json

or you can login to RabbitMq dashboard on below url, and publish msg on queue named ESTERN. make sure you pass header message_id=OFFERS.

http://localhost:15672/
user: guest
password: guest