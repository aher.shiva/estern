package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/aher.shiva/estern/api/controller"
	"gitlab.com/aher.shiva/estern/api/rabbitmq"
	"gitlab.com/aher.shiva/estern/api/repository"
	"gitlab.com/aher.shiva/estern/api/service"
	"gitlab.com/aher.shiva/estern/config"
)

//TODO need to move routes to router.go and initialisation of connection to config init :p
func main() {
	r := gin.Default()
	mysqlDb := config.OpenMysqlConnection()
	rabbitMqConnection := config.OpenRabbitMqConnection()

	hotelRepo := repository.NewHotelRepository(mysqlDb)
	roomRepo := repository.NewRoomRepository(mysqlDb)
	ratePlanRepo := repository.NewRatePlanRepository(mysqlDb)
	offerService := service.NewOfferService(hotelRepo, roomRepo, ratePlanRepo)
	rabbitMqMessageHandlerService := service.NewRabbitMqMessageHandler(offerService)
	publishMessageService := service.NewMessagePublishService(rabbitMqConnection)

	rabbitMqConsumerService := rabbitmq.NewConsumerService(rabbitMqConnection, rabbitMqMessageHandlerService)
	go rabbitMqConsumerService.StartConsumer()

	healthController := controller.HealthController{}
	messagePublishController := controller.MessagePublishController{publishMessageService}
	r.GET("/healthz", healthController.Healthz)
	r.POST("/publish", messagePublishController.PublishMessage)
	r.Run(":5000")
}
