module gitlab.com/aher.shiva/estern

go 1.12

require (
	github.com/NeowayLabs/wabbit v0.0.0-20200916102526-a7d9beea8caa // indirect
	github.com/gin-gonic/gin v1.6.3
	github.com/golang/mock v1.4.4
	github.com/jinzhu/gorm v1.9.16
	github.com/onsi/ginkgo v1.12.0
	github.com/onsi/gomega v1.7.1
	github.com/sirupsen/logrus v1.7.0
	github.com/streadway/amqp v1.0.0
)
