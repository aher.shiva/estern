package rabbitmq

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/aher.shiva/estern/api/common"
	"gitlab.com/aher.shiva/estern/api/service"
)

type ConsumerService interface {
	StartConsumer()
}

type consumerService struct {
	connection            *amqp.Connection
	messageHandlerService service.RabbitMqMessageHandler
}

func NewConsumerService(connection *amqp.Connection, messageHandlerService service.RabbitMqMessageHandler) ConsumerService {
	return &consumerService{connection: connection, messageHandlerService: messageHandlerService}
}

func (service consumerService) StartConsumer() {

	channel, err := service.connection.Channel()
	if err != nil {
		logrus.Error("unable to start rabbitmq consumer")
		panic(err)
	}
	defer channel.Close()

	_, err = channel.QueueDeclare(common.RabbitMqTopicName, false, false, false, false, nil)
	if err != nil {
		logrus.Error("unable to start rabbitmq consumer, error while declaring queue")
		panic(err)
	}

	incommingMsg, err := channel.Consume(common.RabbitMqTopicName, "", true, false, false, false, nil)
	if err != nil {
		logrus.Error("unable to start rabbitmq consumer")
		panic(err)
	}
	forever := make(chan bool)
	go func() {
		for message := range incommingMsg {
			service.messageHandlerService.MessageHandler(message)
		}
	}()

	fmt.Println("successfully started RabbitMQ Instance")
	<-forever
}
