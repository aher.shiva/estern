package repository

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/aher.shiva/estern/api/mocks"
	"gitlab.com/aher.shiva/estern/api/models"
)

func TestRatePlanRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "TestRatePlanRepository")
}

var _ = Describe("TestRatePlanRepository", func() {
	var (
		mockController         *gomock.Controller
		mockDb                 *mocks.MockGormw
		testRatePlanRepository RatePlanRepository
		testContext            context.Context
	)

	BeforeEach(func() {
		mockController = gomock.NewController(GinkgoT())
		mockDb = mocks.NewMockGormw(mockController)
		testRatePlanRepository = NewRatePlanRepository(mockDb)
		testContext = context.Background()
	})

	Context("when Create method gets called with valid rate plan details", func() {

		It("should successfully store rate plan details in DB", func() {
			mockRatePlanRequest := models.RatePlan{
				CancellationPolicy: []models.CancellationPolicy{
					{
						ExpiresDaysBefore: int64(10),
						Type:              "unknown",
					},
				},
				HotelID:    "Hotel 1",
				MealPlan:   "not included",
				Name:       "sabse sata",
				RatePlanID: "1100",
			}

			mockDb.EXPECT().Table("rate_plans").Return(mockDb).Times(1)
			mockDb.EXPECT().Create(&mockRatePlanRequest).Return(mockDb).Times(1)
			mockDb.EXPECT().Error().Return(nil).Times(1)

			actualError := testRatePlanRepository.Create(testContext, mockRatePlanRequest)

			Expect(actualError).To(BeNil())
		})

		It("should return error if error occurred while storing rate plan details in DB", func() {
			mockRatePlanRequest := models.RatePlan{
				CancellationPolicy: []models.CancellationPolicy{
					{
						ExpiresDaysBefore: int64(10),
						Type:              "unknown",
					},
				},
				HotelID:    "Hotel 1",
				MealPlan:   "not included",
				Name:       "sabse sata",
				RatePlanID: "1100",
			}
			mockDb.EXPECT().Table("rate_plans").Return(mockDb).Times(1)
			mockDb.EXPECT().Create(&mockRatePlanRequest).Return(mockDb).Times(1)
			mockDb.EXPECT().Error().Return(errors.New("error occurred while inserting row")).Times(3)
			expectedError := errors.New("error occurred while inserting row")

			actualError := testRatePlanRepository.Create(testContext, mockRatePlanRequest)

			Expect(actualError).To(Equal(expectedError))
		})

	})
})
