package repository

import (
	"context"

	"gitlab.com/aher.shiva/estern/api/common"
	"gitlab.com/aher.shiva/estern/api/models"
)

type RatePlanRepository interface {
	Create(ctxt context.Context, ratePlan models.RatePlan) error
}

type ratePlanRepository struct {
	Db common.Gormw
}

func NewRatePlanRepository(db common.Gormw) RatePlanRepository {
	return &ratePlanRepository{Db: db}
}

func (h *ratePlanRepository) Create(ctxt context.Context, ratePlan models.RatePlan) error {
	log := common.GetLogger(ctxt, "RatePlanRepository", "Create")
	log.Info("storing rate plans in db")
	createRatePlan := h.Db.Table("rate_plans").Create(&ratePlan)
	if createRatePlan.Error() != nil {
		log.Errorf("error occured while inserting ratePlan details, Error: %+v", createRatePlan.Error())
		return createRatePlan.Error()
	}
	log.Info("successfully inserted ratePlan details")
	return nil
}
