package repository

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/aher.shiva/estern/api/mocks"
	"gitlab.com/aher.shiva/estern/api/models"
)

func TestRoomRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Test Room Repository")
}

var _ = Describe("Test Room Repository", func() {
	var (
		mockController     *gomock.Controller
		mockDb             *mocks.MockGormw
		testRoomRepository RoomRepository
		testContext        context.Context
	)

	BeforeEach(func() {
		mockController = gomock.NewController(GinkgoT())
		mockDb = mocks.NewMockGormw(mockController)
		testRoomRepository = NewRoomRepository(mockDb)
		testContext = context.Background()
	})

	Context("when Create method gets called with valid room details", func() {

		It("should successfully store room details in DB", func() {
			mockRoomRequest := models.Room{
				Capacity: models.Capacity{
					ExtraChildren: int64(4),
					MaxAdults:     int64(2),
				},
				Description: "test description",
				HotelID:     "testHotel",
				Name:        "test name",
				RoomID:      "123",
			}
			mockDb.EXPECT().Table("rooms").Return(mockDb).Times(1)
			mockDb.EXPECT().Create(&mockRoomRequest).Return(mockDb).Times(1)
			mockDb.EXPECT().Error().Return(nil).Times(1)

			actualError := testRoomRepository.Create(testContext, mockRoomRequest)

			Expect(actualError).To(BeNil())
		})

		It("should return error if error occurred while storing room details in DB", func() {
			mockRoomRequest := models.Room{
				Capacity: models.Capacity{
					ExtraChildren: int64(4),
					MaxAdults:     int64(2),
				},
				Description: "test description",
				HotelID:     "testHotel",
				Name:        "test name",
				RoomID:      "123",
			}
			mockDb.EXPECT().Table("rooms").Return(mockDb).Times(1)
			mockDb.EXPECT().Create(&mockRoomRequest).Return(mockDb).Times(1)
			mockDb.EXPECT().Error().Return(errors.New("error occurred while inserting row")).Times(3)
			expectedError := errors.New("error occurred while inserting row")

			actualError := testRoomRepository.Create(testContext, mockRoomRequest)

			Expect(actualError).To(Equal(expectedError))
		})

	})
})
