package repository

import (
	"context"

	"gitlab.com/aher.shiva/estern/api/common"
	"gitlab.com/aher.shiva/estern/api/models"
)

type HotelRepository interface {
	Create(ctxt context.Context, hotel models.Hotel) error
}

type hotelRepository struct {
	Db common.Gormw
}

func NewHotelRepository(db common.Gormw) HotelRepository {
	return &hotelRepository{Db: db}
}

func (h *hotelRepository) Create(ctxt context.Context, hotel models.Hotel) error {
	log := common.GetLogger(ctxt, "HotelRepository", "Create")
	log.Info("storing hotel details in db")
	createHotel := h.Db.Table("hotels").Create(&hotel)
	if createHotel.Error() != nil {
		log.Errorf("error occured while inserting hotel details, Error: %+v", createHotel.Error())
		return createHotel.Error()
	}
	log.Info("successfully inserted hotel details")
	return nil
}
