package repository

import (
	"context"

	"gitlab.com/aher.shiva/estern/api/common"
	"gitlab.com/aher.shiva/estern/api/models"
)

type RoomRepository interface {
	Create(ctxt context.Context, room models.Room) error
}

type roomRepository struct {
	Db common.Gormw
}

func NewRoomRepository(db common.Gormw) RoomRepository {
	return &roomRepository{Db: db}
}

func (h *roomRepository) Create(ctxt context.Context, room models.Room) error {
	log := common.GetLogger(ctxt, "RoomRepository", "Create")
	log.Info("storing room details in db")
	createRoom := h.Db.Table("rooms").Create(&room)
	if createRoom.Error() != nil {
		log.Errorf("error occured while inserting room details, Error: %+v", createRoom.Error())
		return createRoom.Error()
	}
	log.Info("successfully inserted room details")
	return nil
}
