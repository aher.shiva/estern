package repository

import (
	"context"
	"errors"
	"testing"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"gitlab.com/aher.shiva/estern/api/mocks"
	"gitlab.com/aher.shiva/estern/api/models"
)

func TestHotelRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Test Hotel Repository")
}

var _ = Describe("Test Hotel Repository", func() {
	var (
		mockController *gomock.Controller
		mockDb         *mocks.MockGormw
		testRepository HotelRepository
		testContext    context.Context
	)

	BeforeEach(func() {
		mockController = gomock.NewController(GinkgoT())
		mockDb = mocks.NewMockGormw(mockController)
		testRepository = NewHotelRepository(mockDb)
		testContext = context.Background()
	})

	Context("when Create method gets called with valid hotel details", func() {

		It("should successfully store hotel details in DB", func() {
			mockHotelRequest := models.Hotel{
				Address:     "address 123",
				Country:     "india",
				Currency:    "INR",
				Description: "test describe",
				HotelID:     "1",
				Latitude:    float64(1111.00),
				Longitude:   float64(1111.00),
				Name:        "hotel nashik",
				RoomCount:   int64(10),
				Telephone:   "12312313",
			}
			mockDb.EXPECT().Table("hotels").Return(mockDb).Times(1)
			mockDb.EXPECT().Create(&mockHotelRequest).Return(mockDb).Times(1)
			mockDb.EXPECT().Error().Return(nil).Times(1)

			actualError := testRepository.Create(testContext, mockHotelRequest)

			Expect(actualError).To(BeNil())
		})

		It("should return error if error occurred while storing hotel details in DB", func() {
			mockHotelRequest := models.Hotel{
				Address:     "address 123",
				Country:     "india",
				Currency:    "INR",
				Description: "test describe",
				HotelID:     "1",
				Latitude:    float64(1111.00),
				Longitude:   float64(1111.00),
				Name:        "hotel nashik",
				RoomCount:   int64(10),
				Telephone:   "12312313",
			}
			mockDb.EXPECT().Table("hotels").Return(mockDb).Times(1)
			mockDb.EXPECT().Create(&mockHotelRequest).Return(mockDb).Times(1)
			mockDb.EXPECT().Error().Return(errors.New("error occurred while inserting row")).Times(3)
			expectedError := errors.New("error occurred while inserting row")

			actualError := testRepository.Create(testContext, mockHotelRequest)

			Expect(actualError).To(Equal(expectedError))
		})

	})
})
