package controller

import (
	"errors"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/gin-gonic/gin"
	"gitlab.com/aher.shiva/estern/api/mocks"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestMessagePublishController(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "TestMessagePublishController")
}

var _ = Describe("TestMessagePublishController", func() {
	var (
		mockController               *gomock.Controller
		responseRecorder             *httptest.ResponseRecorder
		testContext                  *gin.Context
		mockMessagePublishService    *mocks.MockMessagePublishService
		testMessagePublishController MessagePublishController
	)

	BeforeEach(func() {
		mockController = gomock.NewController(GinkgoT())
		responseRecorder = httptest.NewRecorder()
		testContext, _ = gin.CreateTestContext(responseRecorder)
		mockMessagePublishService = mocks.NewMockMessagePublishService(mockController)
		testMessagePublishController = MessagePublishController{mockMessagePublishService}
	})

	Context("when publish message endpoint is called", func() {
		It("should return error if error occurred while publishing message", func() {
			requestBody := `{"offers":[{"cm_offer_id":"8f6995366e854c9faf1d9f3d233702b8","hotel":{"hotel_id":"BH~46456","name":"Hawthorn Suites by Wyndham Eagle CO","country":"US","address":"0315 Chambers Avenue, 81631","latitude":39.660193,"longitude":-106.824123,"telephone":"+1-970-3283000","amenities":["Business Centre","Fitness Room/Gym","Pet Friendly","Disabled Access","Air Conditioned","Free WIFI","Elevator / Lift","Parking"],"description":"Stay a while in beautiful mountain country at this Hawthorn Suites by Wyndham Eagle CO hotel, just off Interstate 70, only 6 miles from the Vail/Eagle Airport and close to skiing, golfing, Eagle River and great restaurants. Pets are welcome at this h","room_count":1,"currency":"USD"},"room":{"hotel_id":"BH~46456","room_id":"S2Q","description":"JUNIOR SUITES WITH 2 QUEEN BEDS","name":"JUNIOR SUITES WITH 2 QUEEN BEDS","capacity":{"max_adults":2,"extra_children":2}},"rate_plan":{"hotel_id":"BH~46456","rate_plan_id":"BAR","cancellation_policy":[{"type":"Free cancellation","expires_days_before":2}],"name":"BEST AVAILABLE RATE","other_conditions":["CXL BY 2 DAYS PRIOR TO ARRIVAL-FEE 1 NIGHT 2 DAYS PRIOR TO ARRIVAL","BEST AVAILABLE RATE"],"meal_plan":"Room only"},"original_data":{"GuaranteePolicy":{"Required":true}},"capacity":{"max_adults":2,"extra_children":2},"number":1,"price":1520,"currency":"USD","check_in":"2020-11-18","check_out":"2020-11-20","fees":[{"type":"CountyTax","description":"COUNTY TAX PER STAY","included":true,"percent":17.5}]}]}`
			url := "/message/publish"
			testContext.Request, _ = http.NewRequest(http.MethodPost, url, strings.NewReader(requestBody))
			mockMessagePublishService.EXPECT().PublishMessage(testContext, []byte(requestBody)).Return(errors.New("error occurred while publishing msg")).Times(1)
			expectedError := `{"errorCode":"INTERNAL_SERVER_ERROR"}`

			testMessagePublishController.PublishMessage(testContext)

			Expect(responseRecorder.Code).To(Equal(500))
			Expect(responseRecorder.Body.String()).To(Equal(expectedError))
		})

		It("should status success if message succesfully published", func() {
			requestBody := `{"offers":[{"cm_offer_id":"8f6995366e854c9faf1d9f3d233702b8","hotel":{"hotel_id":"BH~46456","name":"Hawthorn Suites by Wyndham Eagle CO","country":"US","address":"0315 Chambers Avenue, 81631","latitude":39.660193,"longitude":-106.824123,"telephone":"+1-970-3283000","amenities":["Business Centre","Fitness Room/Gym","Pet Friendly","Disabled Access","Air Conditioned","Free WIFI","Elevator / Lift","Parking"],"description":"Stay a while in beautiful mountain country at this Hawthorn Suites by Wyndham Eagle CO hotel, just off Interstate 70, only 6 miles from the Vail/Eagle Airport and close to skiing, golfing, Eagle River and great restaurants. Pets are welcome at this h","room_count":1,"currency":"USD"},"room":{"hotel_id":"BH~46456","room_id":"S2Q","description":"JUNIOR SUITES WITH 2 QUEEN BEDS","name":"JUNIOR SUITES WITH 2 QUEEN BEDS","capacity":{"max_adults":2,"extra_children":2}},"rate_plan":{"hotel_id":"BH~46456","rate_plan_id":"BAR","cancellation_policy":[{"type":"Free cancellation","expires_days_before":2}],"name":"BEST AVAILABLE RATE","other_conditions":["CXL BY 2 DAYS PRIOR TO ARRIVAL-FEE 1 NIGHT 2 DAYS PRIOR TO ARRIVAL","BEST AVAILABLE RATE"],"meal_plan":"Room only"},"original_data":{"GuaranteePolicy":{"Required":true}},"capacity":{"max_adults":2,"extra_children":2},"number":1,"price":1520,"currency":"USD","check_in":"2020-11-18","check_out":"2020-11-20","fees":[{"type":"CountyTax","description":"COUNTY TAX PER STAY","included":true,"percent":17.5}]}]}`
			url := "/message/publish"
			testContext.Request, _ = http.NewRequest(http.MethodPost, url, strings.NewReader(requestBody))
			mockMessagePublishService.EXPECT().PublishMessage(testContext, []byte(requestBody)).Return(nil).Times(1)
			expectedError := `{"status":"success"}`

			testMessagePublishController.PublishMessage(testContext)

			Expect(responseRecorder.Code).To(Equal(200))
			Expect(responseRecorder.Body.String()).To(Equal(expectedError))
		})
	})
})
