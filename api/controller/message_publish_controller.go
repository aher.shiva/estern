package controller

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/aher.shiva/estern/api/service"
)

type MessagePublishController struct {
	MessagePublishService service.MessagePublishService
}

func (c MessagePublishController) PublishMessage(ctxt *gin.Context) {

	messageBody, err := ctxt.GetRawData()
	if err != nil {
		ctxt.AbortWithStatusJSON(400, gin.H{
			"errorCode": "BAD_REQUEST",
		})
		return
	}

	err = c.MessagePublishService.PublishMessage(ctxt, messageBody)
	if err != nil {
		ctxt.AbortWithStatusJSON(500, gin.H{
			"errorCode": "INTERNAL_SERVER_ERROR",
		})
		return
	}

	ctxt.JSON(200, gin.H{
		"status": "success",
	})
}
