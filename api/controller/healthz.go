package controller

import (
	"github.com/gin-gonic/gin"
)

type HealthController struct {
}

func (c HealthController) Healthz(ctxt *gin.Context) {
	ctxt.JSON(200, gin.H{
		"status": "up",
	})
}
