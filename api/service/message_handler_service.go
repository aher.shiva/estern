package service

import (
	"context"
	"encoding/json"
	"errors"

	"github.com/sirupsen/logrus"
	"github.com/streadway/amqp"
	"gitlab.com/aher.shiva/estern/api/common"
	"gitlab.com/aher.shiva/estern/api/models"
)

type RabbitMqMessageHandler interface {
	MessageHandler(message amqp.Delivery) error
}

type rabbitMqMessageHandler struct {
	offerService OfferService
}

func NewRabbitMqMessageHandler(offerService OfferService) RabbitMqMessageHandler {
	return &rabbitMqMessageHandler{offerService: offerService}
}

func (service rabbitMqMessageHandler) MessageHandler(message amqp.Delivery) error {
	switch message.MessageId {
	case common.MessageIdOffers:
		ctxt := context.Background()
		log := common.GetLogger(ctxt, "RabbitMqConsumerService", "messageHandler")
		log.Info("received new offer message")

		var request models.OfferRequest
		err := json.Unmarshal(message.Body, &request)
		if err != nil {
			log.Errorf("error occurred while unmarshalling incomming message, Error: %+v", err)
			return err
		}

		for _, offer := range request.Offers {
			err = service.offerService.StoreOffer(ctxt, offer)
			if err != nil {
				log.Errorf("error occurred while storing offer in db, Error: %+v", err)
				return err
			}
		}
		return nil
	default:
		logrus.Errorf("unknown message of type: %s", message.MessageId)
		return errors.New("unknown message type")
	}
}
