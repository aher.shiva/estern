package service

import (
	"context"
	"errors"
	"testing"

	"gitlab.com/aher.shiva/estern/api/mocks"
	"gitlab.com/aher.shiva/estern/api/models"

	"github.com/golang/mock/gomock"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
)

func TestOffersServiceRepository(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "TestOffersServiceRepository")
}

var _ = Describe("TestOffersServiceRepository", func() {
	var (
		mockController         *gomock.Controller
		mockHotelRepository    *mocks.MockHotelRepository
		mockRoomRepository     *mocks.MockRoomRepository
		mockRatePlanRepository *mocks.MockRatePlanRepository
		testContext            context.Context
		testOfferService       OfferService
	)

	BeforeEach(func() {
		mockController = gomock.NewController(GinkgoT())
		mockHotelRepository = mocks.NewMockHotelRepository(mockController)
		mockRoomRepository = mocks.NewMockRoomRepository(mockController)
		mockRatePlanRepository = mocks.NewMockRatePlanRepository(mockController)
		testContext = context.Background()
		testOfferService = NewOfferService(mockHotelRepository, mockRoomRepository, mockRatePlanRepository)
	})

	Context("when StoreOffer method is called with valid offer details", func() {

		It("should successfully store hotels, room and ratePlan details in db", func() {
			mockHotelRequest := models.Hotel{
				Address:     "address 123",
				Country:     "india",
				Currency:    "INR",
				Description: "test describe",
				HotelID:     "1",
				Latitude:    float64(1111.00),
				Longitude:   float64(1111.00),
				Name:        "hotel nashik",
				RoomCount:   int64(10),
				Telephone:   "12312313",
			}
			mockRoomRequest := models.Room{
				Capacity: models.Capacity{
					ExtraChildren: int64(4),
					MaxAdults:     int64(2),
				},
				Description: "test description",
				HotelID:     "testHotel",
				Name:        "test name",
				RoomID:      "123",
			}
			mockRatePlanRequest := models.RatePlan{
				CancellationPolicy: []models.CancellationPolicy{
					{
						ExpiresDaysBefore: int64(10),
						Type:              "unknown",
					},
				},
				HotelID:    "Hotel 1",
				MealPlan:   "not included",
				Name:       "sabse sata",
				RatePlanID: "1100",
			}
			mockOfferRequest := models.Offer{
				Hotel:    mockHotelRequest,
				Room:     mockRoomRequest,
				RatePlan: mockRatePlanRequest,
			}
			mockHotelRepository.EXPECT().Create(testContext, mockHotelRequest).Return(nil).Times(1)
			mockRoomRepository.EXPECT().Create(testContext, mockRoomRequest).Return(nil).Times(1)
			mockRatePlanRepository.EXPECT().Create(testContext, mockRatePlanRequest).Return(nil).Times(1)

			actualError := testOfferService.StoreOffer(testContext, mockOfferRequest)

			Expect(actualError).To(BeNil())
		})

		It("should return error if error occurred while storing hotel details in DB", func() {
			mockHotelRequest := models.Hotel{
				Address:     "address 123",
				Country:     "india",
				Currency:    "INR",
				Description: "test describe",
				HotelID:     "1",
				Latitude:    float64(1111.00),
				Longitude:   float64(1111.00),
				Name:        "hotel nashik",
				RoomCount:   int64(10),
				Telephone:   "12312313",
			}
			mockRoomRequest := models.Room{
				Capacity: models.Capacity{
					ExtraChildren: int64(4),
					MaxAdults:     int64(2),
				},
				Description: "test description",
				HotelID:     "testHotel",
				Name:        "test name",
				RoomID:      "123",
			}
			mockRatePlanRequest := models.RatePlan{
				CancellationPolicy: []models.CancellationPolicy{
					{
						ExpiresDaysBefore: int64(10),
						Type:              "unknown",
					},
				},
				HotelID:    "Hotel 1",
				MealPlan:   "not included",
				Name:       "sabse sata",
				RatePlanID: "1100",
			}
			mockOfferRequest := models.Offer{
				Hotel:    mockHotelRequest,
				Room:     mockRoomRequest,
				RatePlan: mockRatePlanRequest,
			}
			mockHotelRepository.EXPECT().Create(testContext, mockHotelRequest).Return(errors.New("error occurred while storing hotel details in DB")).Times(1)
			expectedError := errors.New("error occurred while storing hotel details in DB")

			actualError := testOfferService.StoreOffer(testContext, mockOfferRequest)

			Expect(actualError).To(Equal(expectedError))
		})

		It("should return error if error occurred while storing room details in DB", func() {
			mockHotelRequest := models.Hotel{
				Address:     "address 123",
				Country:     "india",
				Currency:    "INR",
				Description: "test describe",
				HotelID:     "1",
				Latitude:    float64(1111.00),
				Longitude:   float64(1111.00),
				Name:        "hotel nashik",
				RoomCount:   int64(10),
				Telephone:   "12312313",
			}
			mockRoomRequest := models.Room{
				Capacity: models.Capacity{
					ExtraChildren: int64(4),
					MaxAdults:     int64(2),
				},
				Description: "test description",
				HotelID:     "testHotel",
				Name:        "test name",
				RoomID:      "123",
			}
			mockRatePlanRequest := models.RatePlan{
				CancellationPolicy: []models.CancellationPolicy{
					{
						ExpiresDaysBefore: int64(10),
						Type:              "unknown",
					},
				},
				HotelID:    "Hotel 1",
				MealPlan:   "not included",
				Name:       "sabse sata",
				RatePlanID: "1100",
			}
			mockOfferRequest := models.Offer{
				Hotel:    mockHotelRequest,
				Room:     mockRoomRequest,
				RatePlan: mockRatePlanRequest,
			}
			mockHotelRepository.EXPECT().Create(testContext, mockHotelRequest).Return(nil).Times(1)
			mockRoomRepository.EXPECT().Create(testContext, mockRoomRequest).Return(errors.New("error occurred while storing room details in DB")).Times(1)
			expectedError := errors.New("error occurred while storing room details in DB")

			actualError := testOfferService.StoreOffer(testContext, mockOfferRequest)

			Expect(actualError).To(Equal(expectedError))
		})

		It("should return error if error occurred while storing hotel details in DB", func() {
			mockHotelRequest := models.Hotel{
				Address:     "address 123",
				Country:     "india",
				Currency:    "INR",
				Description: "test describe",
				HotelID:     "1",
				Latitude:    float64(1111.00),
				Longitude:   float64(1111.00),
				Name:        "hotel nashik",
				RoomCount:   int64(10),
				Telephone:   "12312313",
			}
			mockRoomRequest := models.Room{
				Capacity: models.Capacity{
					ExtraChildren: int64(4),
					MaxAdults:     int64(2),
				},
				Description: "test description",
				HotelID:     "testHotel",
				Name:        "test name",
				RoomID:      "123",
			}
			mockRatePlanRequest := models.RatePlan{
				CancellationPolicy: []models.CancellationPolicy{
					{
						ExpiresDaysBefore: int64(10),
						Type:              "unknown",
					},
				},
				HotelID:    "Hotel 1",
				MealPlan:   "not included",
				Name:       "sabse sata",
				RatePlanID: "1100",
			}
			mockOfferRequest := models.Offer{
				Hotel:    mockHotelRequest,
				Room:     mockRoomRequest,
				RatePlan: mockRatePlanRequest,
			}
			mockHotelRepository.EXPECT().Create(testContext, mockHotelRequest).Return(nil).Times(1)
			mockRoomRepository.EXPECT().Create(testContext, mockRoomRequest).Return(nil).Times(1)
			mockRatePlanRepository.EXPECT().Create(testContext, mockRatePlanRequest).Return(errors.New("error occurred while storing rate plan details in DB")).Times(1)
			expectedError := errors.New("error occurred while storing rate plan details in DB")

			actualError := testOfferService.StoreOffer(testContext, mockOfferRequest)

			Expect(actualError).To(Equal(expectedError))
		})
	})
})
