package service

import (
	"context"

	"github.com/streadway/amqp"
	"gitlab.com/aher.shiva/estern/api/common"
)

type MessagePublishService interface {
	PublishMessage(ctxt context.Context, message []byte) error
}

type messagePublishService struct {
	connection *amqp.Connection
}

func NewMessagePublishService(connection *amqp.Connection) MessagePublishService {
	return &messagePublishService{connection: connection}
}

func (service messagePublishService) PublishMessage(ctxt context.Context, message []byte) error {
	log := common.GetLogger(ctxt, "ProducerService", "PublishMessage")
	log.Info("publishing message to rabbitmq queue")

	channel, err := service.connection.Channel()
	if err != nil {
		log.Errorf("error occured while creating channel, Error: %+v", err)
		return err
	}
	defer channel.Close()

	err = channel.Publish(
		"",
		common.RabbitMqTopicName,
		false,
		false,
		amqp.Publishing{
			ContentType: "text/plain",
			MessageId:   common.MessageIdOffers,
			Body:        message,
		},
	)
	if err != nil {
		log.Errorf("error occured while publishing message, Error: %+v", err)
		return err
	}

	log.Info("successfully published message to rabbitmq queue")
	return nil
}
