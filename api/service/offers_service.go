package service

import (
	"context"

	"gitlab.com/aher.shiva/estern/api/common"
	"gitlab.com/aher.shiva/estern/api/models"
	"gitlab.com/aher.shiva/estern/api/repository"
)

type OfferService interface {
	StoreOffer(ctxt context.Context, offer models.Offer) error
}

type offerService struct {
	hotelRepo    repository.HotelRepository
	roomRepo     repository.RoomRepository
	ratePlanRepo repository.RatePlanRepository
}

func NewOfferService(hotelRepo repository.HotelRepository, roomRepo repository.RoomRepository, ratePlanRepo repository.RatePlanRepository) OfferService {
	return &offerService{hotelRepo: hotelRepo, roomRepo: roomRepo, ratePlanRepo: ratePlanRepo}
}

func (service offerService) StoreOffer(ctxt context.Context, offer models.Offer) error {
	log := common.GetLogger(ctxt, "OfferService", "StoreOffer")
	log.Info("storing offer details in db")

	err := service.hotelRepo.Create(ctxt, offer.Hotel)
	if err != nil {
		log.Errorf("error occured while inserting hotel details, Error: %+v", err)
		return err
	}
	log.Info("successfully stored hotel details in db")

	err = service.roomRepo.Create(ctxt, offer.Room)
	if err != nil {
		log.Errorf("error occured while inserting room details, Error: %+v", err)
		return err
	}
	log.Info("successfully stored room details in db")

	err = service.ratePlanRepo.Create(ctxt, offer.RatePlan)
	if err != nil {
		log.Errorf("error occured while inserting rate plan details, Error: %+v", err)
		return err
	}
	log.Info("successfully stored rate plan details in db")

	log.Info("successfully stored offer details in db")

	return nil
}
