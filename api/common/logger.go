package common

import (
	"context"

	log "github.com/sirupsen/logrus"
)

//TODO: future scope may be adding traceID to context and capturing it in logs for request tracing
func GetLogger(ctxt context.Context, class, method string) *log.Entry {
	return log.WithFields(log.Fields{
		"class":  class,
		"method": method,
	})
}
