package models

type OfferRequest struct {
	Offers []Offer `json:"offers"`
}

type Offer struct {
	Hotel    Hotel    `json:"hotel"`
	Room     Room     `json:"room"`
	RatePlan RatePlan `json:"rate_plan"`
}
