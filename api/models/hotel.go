package models

import "github.com/jinzhu/gorm"

type Hotel struct {
	gorm.Model
	Address string `json:"address" gorm:"address"`
	//Amenities   []string `json:"amenities" gorm:"amenities"`
	Country     string  `json:"country" gorm:"country"`
	Currency    string  `json:"currency" gorm:"currency"`
	Description string  `json:"description" gorm:"description"`
	HotelID     string  `json:"hotel_id" gorm:"hotel_id"`
	Latitude    float64 `json:"latitude" gorm:"latitude"`
	Longitude   float64 `json:"longitude" gorm:"longitude"`
	Name        string  `json:"name" gorm:"name"`
	RoomCount   int64   `json:"room_count" gorm:"room_count"`
	Telephone   string  `json:"telephone" gorm:"telephone"`
}
