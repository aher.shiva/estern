package models

import "github.com/jinzhu/gorm"

type RatePlan struct {
	gorm.Model
	CancellationPolicy []CancellationPolicy `json:"cancellation_policy"`
	HotelID            string               `json:"hotel_id" gorm:"HotelID"`
	MealPlan           string               `json:"meal_plan" gorm:"MealPlan"`
	Name               string               `json:"name" gorm:"Name"`
	//OtherConditions    []string             `json:"other_conditions" gorm:"extra_children"`
	RatePlanID string `json:"rate_plan_id" gorm:"RatePlanID"`
}

type CancellationPolicy struct {
	gorm.Model
	ExpiresDaysBefore int64  `json:"expires_days_before" gorm:"ExpiresDaysBefore"`
	Type              string `json:"type" gorm:"Type"`
}
