package models

import "github.com/jinzhu/gorm"

type Room struct {
	gorm.Model
	Capacity    Capacity `json:"capacity"`
	Description string   `json:"description" gorm:"description"`
	HotelID     string   `json:"hotel_id" gorm:"hotel_id"`
	Name        string   `json:"name" gorm:"name"`
	RoomID      string   `json:"room_id" gorm:"room_id"`
}

type Capacity struct {
	gorm.Model
	ExtraChildren int64 `json:"extra_children" gorm:"extra_children"`
	MaxAdults     int64 `json:"max_adults" gorm:"max_adults"`
}
